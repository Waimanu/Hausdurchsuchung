# Hausdurchsuchung

Allgemeine infos für Hausdurchsuchung

# Checkliste

[] Min 5. Beamte
[] Neutraler Zeuge
[] Abschrift des Durchsuchungsbeschluss
[] Durchsuchungsgrund ist begrenzt
[] Anwalt kontaktiert
[] Der maßnahme wiedersprechen
[] Aufzeichnen und Kopieren
[] Versiegelung von Dokumenten

# Allgemein

1. Ruhe bewahren
2. Durchsuchungsbeschluss vorlegen lassen
2.1. In ruhe Lesen
2.2. Dursuchungsgrund prüfen
2.3. Verhältnismessigkeiten prüfen
3. Sind ausreichend Beamte und Zeugen da?
4. Klappe halten
5. Anwalt hinzurufen
6. Es besteht keine Mitwirkungspflicht
7. Der Maßnahme Widersprechen!
8. So viel wie möglich Privat Aufzeichnen
9. Dokumente sollen Versiegelt werden

# Quellen

1. https://de.wikipedia.org/wiki/Hausdurchsuchung
